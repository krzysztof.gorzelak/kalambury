#!/bin/bash

# Initialize an empty array to hold the file information
declare -a file_list

# Iterate over the files in the words/ directory
for file in words/*; do
    # Extract the filename without the directory
    filename=$(basename "$file")
    
    # Extract the category and difficulty from the filename
    category=$(echo "$filename" | cut -d'-' -f1)
    difficulty=$(echo "$filename" | cut -d'-' -f2 | cut -d'.' -f1)
    
    # Replace underscores with spaces in the category
    category=${category//_/ }
    
    # Add the file information to the array
    file_list+=("{\"file\": \"$filename\", \"description\": \"$category\", \"difficulty\": $difficulty}")
done

# Create the JSON output
json_output="{\"lists\": [$(IFS=,; echo "${file_list[*]}")]}"

# Write the JSON output to lists.json
echo "$json_output" > lists.json

# Validate the structure of lists.json
if jq empty lists.json; then
    echo "lists.json is valid JSON."
else
    echo "lists.json is invalid JSON." >&2
    exit 1
fi

echo "lists.json has been generated and validated based on the content of the words/ folder."
