window.addEventListener('load', () => {
    const playButton = document.getElementById('play-button');
    const wordMenu = document.getElementById('word-menu');
    const difficultyMenu = document.getElementById('difficulty-menu');
    
    let wordCache = {}; // Cache for word lists
    let currentList = '';
    let lists = []; // To store all lists
    let displayedWords = new Set(); // To keep track of displayed words

    async function loadWordLists() {
        const response = await fetch('lists.json');
        const data = await response.json();
        lists = data.lists;
        // Select difficulty 1 by default and load related lists
        const defaultDifficultyButton = difficultyMenu.querySelector('button[data-difficulty="1"]');
        defaultDifficultyButton.classList.add('selected');
        updateWordMenu(1);
        // Automatically select and load the first list
        const firstButton = ul.querySelector('button');
        if (firstButton) {
            firstButton.click();
        }
    }

    function displayWord(word) {
        const textBox = document.getElementById('text-box');
        textBox.textContent = word; // Display the word in the div
    }

    async function fetchWords(list) {
        if (!wordCache[list]) {
            const response = await fetch(`words/${list}`);
            const words = await response.text();
            wordCache[list] = words.split('\n').filter(word => word.trim() !== '');
        }
        return wordCache[list];
    }

    function updateWordMenu(difficulty) {
        wordMenu.innerHTML = ''; // Clear existing options
        const ul = document.createElement('ul');
        wordMenu.appendChild(ul);
        lists.filter(list => list.difficulty === difficulty).forEach(list => {
            const li = document.createElement('li');
            const button = document.createElement('button');
            button.value = list.file;
            button.textContent = list.description;
            button.addEventListener('click', async () => {
                document.querySelectorAll('#word-menu button').forEach(btn => btn.classList.remove('selected'));
                button.classList.add('selected');
                currentList = list.file;
                const wordArray = await fetchWords(currentList);
                displayWord(wordArray[0]); // Display the first word immediately
            });
            li.appendChild(button);
            ul.appendChild(li);
        });
    }

    difficultyMenu.addEventListener('click', (event) => {
        if (event.target.tagName === 'BUTTON') {
            document.querySelectorAll('#difficulty-menu button').forEach(btn => btn.classList.remove('selected'));
            event.target.classList.add('selected');
            const difficulty = parseInt(event.target.getAttribute('data-difficulty'), 10);
            updateWordMenu(difficulty);
        }
    });

    playButton.addEventListener('click', async () => {
        if (currentList === '') {
            alert('Proszę wybrać listę haseł!');
            return;
        }
        const wordArray = await fetchWords(currentList);
        let randomWord;
        do {
            randomWord = wordArray[Math.floor(Math.random() * wordArray.length)];
        } while (displayedWords.has(randomWord) && displayedWords.size < wordArray.length);

        displayedWords.add(randomWord);
        displayWord(randomWord);

        // Reset the set if all words have been displayed
        if (displayedWords.size === wordArray.length) {
            displayedWords.clear();
        }
    });


    loadWordLists(); // Load word lists on page load    
});
