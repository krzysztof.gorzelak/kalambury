# Kalambury

## Name
Kalambury

## Description
Prosta gra / strona do wyświetlania haseł do gry w kalambury. Hasła zostały wygenerowane przy użyciu modelu LLM.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
