#!/bin/bash

# Sprawdź, czy katalog "words" istnieje, jeśli nie - utwórz go
mkdir -p words

# Wczytaj kategorie z pliku categories.txt
categories=$(cat categories.txt)

# Funkcja generująca pliki z listami słów
generate_files() {
    local category=$1
    for difficulty in {1..3}; do
        # Wczytaj prompt dla danej trudności
        prompt=$(cat "prompt${difficulty}.txt")
        
        # Zamień {category} na aktualną kategorię
        prompt=${prompt//"{category}"/$category}
        
        # Uruchom chatblade i zapisz wynik do odpowiedniego pliku
        chatblade -o -n "$prompt" > "words/${category}-${difficulty}.txt"
    done
}

# Dla każdej kategorii wywołaj funkcję generującą pliki
while IFS= read -r category; do
    echo "$category"
    generate_files "$category"
done <<< "$categories"

echo "Pliki zostały wygenerowane w katalogu 'words'."
