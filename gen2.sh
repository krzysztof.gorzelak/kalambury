#!/bin/bash

# Sprawdź, czy katalog "words" istnieje, jeśli nie - utwórz go
mkdir -p words

# Wczytaj kategorie z pliku categories.txt do tablicy
mapfile -t categories < categories.txt


# Funkcja generująca pliki z listami słów
generate_files() {
    local category=$1
    local category_underscore=${category// /_}
    echo "Processing category: $category"
    for difficulty in {1..3}; do
        echo "  Generating words for difficulty level: $difficulty"
        # Wczytaj prompt dla danej trudności
        prompt=$(cat "prompt${difficulty}.txt")
        
        # Zamień {category} na aktualną kategorię
        prompt=${prompt//"{category}"/$category}
        
        # Uruchom chatblade i zapisz wynik do pliku tymczasowego
        temp_file=$(mktemp)
        chatblade -o -n "$prompt" > "$temp_file"
        
        # Usuń numerację z pliku tymczasowego i zapisz do docelowego pliku
        filename="words/${category_underscore}-${difficulty}.txt"
        awk '{ $1=""; sub(/^ /, ""); print }' "$temp_file" > "$filename"
        
        # Usuń plik tymczasowy
        rm "$temp_file"

    done
}

# Dla każdej kategorii wywołaj funkcję generującą pliki
for category in "${categories[@]}"; do
    generate_files "$category"
done
echo "All categories have been processed."


echo "Pliki zostały wygenerowane w katalogu 'words'."
